package main

import (
	"log"
	"os"
	"path"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"gitlab.com/VagueCoder0to.n/Trading-Client-Project/Test-Data-Source/app/database-client/loader"
)

var projectHome string

func main() {
	projectHome = os.Getenv("PROJECT_HOME")
	files, err := loader.ListDir(path.Join(projectHome, "app", "database-client", "datasets"))
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		loader.ReadCSV(file)
	}

	gorm.Dialect("postgres")
}
