package loader

type Company struct {
	// gorm.Model

	Name    string `json:"company_name"`
	Acronym string `json:"company_code"`
}

type Record struct {
	// gorm.Model

	CompanyID           int    `json:"-"`
	Date                string `json:"date" csv:"Date"`
	OpenPrice           string `json:"open_price" csv:"Open Price"`
	HighPrice           string `json:"high_price" csv:"High Price"`
	LowPrice            string `json:"low_price" csv:"Low Price"`
	ClosePrice          string `json:"close_price" csv:"Close Price"`
	WAP                 string `json:"wap" csv:"WAP"`
	NumberOfShares      string `json:"number_of_shares" csv:"No.of Shares"`
	NumberOfTrades      string `json:"number_of_trades" csv:"No. of Trades"`
	Turnover            string `json:"turnover" csv:"Total Turnover (Rs.)"`
	DeliverableQuantity string `json:"deliverable_quantity" csv:"Deliverable Quantity"`
	Percentage          string `json:"percentage" csv:"% Deli. Qty to Traded Qty"`
	SpreadHighLow       string `json:"spread_high_low" csv:"Spread High-Low"`
	SpreadCloseOpen     string `json:"spread_close_open" csv:"Spread Close-Open"`
}
