package loader

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"

	"github.com/gocarina/gocsv"
)

var ProjectHome string

type Loader struct {
	*log.Logger
}

// NewLoader initializes Loader object
func NewLoader(l *log.Logger) *Loader {
	ProjectHome = os.Getenv("PROJECT_HOME")
	return &Loader{l}
}

// ListDir lists out the files in a directory path and return file onjects
func ListDir(dirPath string) ([]*os.File, error) {
	fileInfos, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}

	var files []*os.File
	for _, f := range fileInfos {
		fullname := path.Join(dirPath, f.Name())
		file, err := os.OpenFile(fullname, os.O_RDONLY, 0666)
		if err == nil {
			files = append(files, file)
		}
	}

	if len(files) == 0 {
		return nil, fmt.Errorf("No files found or found inaccessible")
	}
	return files, nil
}

func ReadCSV(file *os.File) error {
	records := []*Record{}
	err := gocsv.Unmarshal(file, &records)
	if err != nil {
		return err
	}

	err = json.NewEncoder(os.Stdout).Encode(records)
	if err != nil {
		return err
	}

	return nil
}
