module gitlab.com/VagueCoder0to.n/Trading-Client-Project/Test-Data-Source

go 1.13

require (
	github.com/gocarina/gocsv v0.0.0-20201208093247-67c824bc04d4
	github.com/jinzhu/gorm v1.9.16
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/text v0.3.3 // indirect
)
