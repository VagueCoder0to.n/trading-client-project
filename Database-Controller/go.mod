module gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller

go 1.13

require (
	github.com/sirupsen/logrus v1.8.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.3.0
)
