package logger

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"path"
	"time"

	"github.com/spf13/viper"
)

// getFilenames function reads the config file and returns the INFO, WARN and ERRO log files.
func getFilenames() (*FileObjects, error) {
	viper.SetConfigFile(configFile)
	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}
	files := make(FileObjects)
	for _, v := range []string{"INFO", "WARN", "ERRO"} {
		text, ok := viper.Get(v).(string)
		if !ok {
			return nil, fmt.Errorf("'%s' is not found in %s", v, configFile)
		}
		files[v] = &file{}
		files[v].path = text
	}
	return &files, nil

}

// logFiles function creates log files for INFO, WARN, ERRO and also a default
func createLogFiles() (*FileObjects, error) {
	defaultLogsPath := logDir + "default.log"
	defaultLogs, err := os.OpenFile(defaultLogsPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return nil, err
	}
	defer defaultLogs.Close()

	defaultLogger := log.New(defaultLogs, "[Default Logs] ", log.LstdFlags|log.Lshortfile)

	// Reading env vars from custom config
	files, err := getFilenames()
	if err != nil {
		defaultLogger.Printf("Config file (app/config/.env) not found: %v", err)
		log.Fatalf("Config file (app/config/.env) not found: %v", err)
	}

	for _, rec := range *files {
		file, err := os.OpenFile(rec.path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			defaultLogger.Printf("%v Logger File Creation Error: %v", rec, err)
			return nil, err
		}
		rec.file = file
	}
	(*files)["DEF"] = &file{defaultLogsPath, defaultLogs}
	return files, nil
}

func addTimestamp(files *FileObjects, sw *BiDiSignals, renameFlag bool) {
	signal.Notify(*sw.Send, os.Interrupt)
	go func(recv chan<- bool) {
		<-*sw.Send
		for _, file := range *files {
			filepath, filename := path.Split(file.path)
			ext := path.Ext(filename)
			name := filename[:len(filename)-len(ext)]
			newPath := (filepath + "logs/" + name +
				fmt.Sprintf("_logs_exported_at_%v",
					time.Now().Format("2006_01_02_03_04_05_PM_MST")) + ext)
			if renameFlag {
				os.Rename(file.path, newPath)
			}
			file.path = newPath
		}
		recv <- true
	}(*sw.Recv)
}
