package logger

import "fmt"

// Info method for logger struct
func (c *Logger) Info(args ...interface{}) {
	c.Standard.Info(fmt.Sprintln(args...))
	c.File.info.Info(fmt.Sprintln(args...))
}

// Warning method for logger struct
func (c *Logger) Warn(args ...interface{}) {
	c.Standard.Warn(fmt.Sprintln(args...))
	c.File.warn.Warn(fmt.Sprintln(args...))
}

// Error method for logger struct
func (c *Logger) Error(args ...interface{}) {
	c.Standard.Error(fmt.Sprintln(args...))
	c.File.erro.Error(fmt.Sprintln(args...))
}
