package logger

import (
	"fmt"
	"log"
	"os"
	"runtime"

	"github.com/sirupsen/logrus"
)

// ConfigPath consists of the Config file's directory location
var (
	home       string
	configFile string
	logDir     string
)

// logger struct contains all logging objects
type Logger struct {
	*log.Logger
	Standard *logrus.Entry
	File     *fileLoggers
	Signals  *BiDiSignals
}

// fileLoggers struct contain all the loggers that write on log files
type fileLoggers struct {
	info *logrus.Entry
	warn *logrus.Entry
	erro *logrus.Entry
}

type file struct {
	path string
	file *os.File
}

type BiDiSignals struct {
	Send *chan os.Signal
	Recv *chan bool
}

type FileObjects map[string]*file

func NewBiDiSignals() *BiDiSignals {
	home = os.Getenv("GOPATH") + "/src/gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller"
	configFile = home + "/app/config/.env"
	logDir = home + "/app/logger/"

	send := make(chan os.Signal, 1)
	recv := make(chan bool, 1)

	return &BiDiSignals{
		Send: &send,
		Recv: &recv,
	}
}

// NewLogger initiates logger object
func NewLogger() *Logger {
	signals := NewBiDiSignals()
	loggerFiles, err := createLogFiles()
	if err != nil {
		log.Fatalf("Logger Files Creation Error: %v", err)
	}

	fmt.Println("Here")

	addTimestamp(loggerFiles, signals, true)
	_, callerFile, line, _ := runtime.Caller(1)

	stdOutLogger := logrus.NewEntry(&logrus.Logger{
		Out: os.Stderr,
		Formatter: &logrus.TextFormatter{
			FullTimestamp: true,
		},
		Level:        logrus.GetLevel(),
		ExitFunc:     os.Exit,
		ReportCaller: false,
	}).WithField("file", fmt.Sprintf("%v: %v", callerFile, line))

	infoFileLogger := logrus.NewEntry(&logrus.Logger{
		Out: (*loggerFiles)["INFO"].file,
		Formatter: &logrus.JSONFormatter{
			PrettyPrint: true,
		},
		Level:        logrus.GetLevel(),
		ExitFunc:     os.Exit,
		ReportCaller: false,
	}).WithField("file", fmt.Sprintf("%v: %v", callerFile, line))

	warnFileLogger := logrus.NewEntry(&logrus.Logger{
		Out: (*loggerFiles)["WARN"].file,
		Formatter: &logrus.JSONFormatter{
			PrettyPrint: true,
		},
		Level:        logrus.GetLevel(),
		ExitFunc:     os.Exit,
		ReportCaller: false,
	}).WithField("file", fmt.Sprintf("%v: %v", callerFile, line))

	erroFileLogger := logrus.NewEntry(&logrus.Logger{
		Out: (*loggerFiles)["ERRO"].file,
		Formatter: &logrus.JSONFormatter{
			PrettyPrint: true,
		},
		Level:        logrus.GetLevel(),
		ExitFunc:     os.Exit,
		ReportCaller: false,
	}).WithField("file", fmt.Sprintf("%v: %v", callerFile, line))

	return &Logger{
		Standard: stdOutLogger,
		File: &fileLoggers{
			info: infoFileLogger,
			warn: warnFileLogger,
			erro: erroFileLogger,
		},
		Signals: signals,
	}
}
