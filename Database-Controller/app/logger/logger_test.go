package logger

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	deleteList []string
	logger     *Logger
)

func TestConfig(t *testing.T) {
	files, err := getFilenames()
	if err != nil {
		assert.Error(t, err, "Config File Not Found or Not Readable")
	} else {
		msg := "Key '%q' missing in config file (app/config/.env)"
		for _, tag := range []string{"INFO", "WARN", "ERROR"} {
			assert.NotEqual(t, (*files)[tag].path, "", msg, tag)
		}
	}
}

func TestSigWait(t *testing.T) {
	sw := NewSigWait()
	assert.NotNil(t, sw, "SigWait Object shouldn't be Nil.")
}

func TestInitLogger(t *testing.T) {
	logger = NewLogger()
	assert.NotNil(t, logger, "Logger Object shouldn't be Nil.")
}

// deleteFiles to remove the list of files
func deleteFiles(files *FileObjects) {
	for _, file := range *files {
		os.Remove(file.path)
	}
}

func TestLogFiles(t *testing.T) {
	// Since default logs file's name was added later in the logger.go
	files, err := getFilenames()
	(*files)["DIF"] = &file{logDir + "default.log", nil}

	// Delete pre-existing log files
	deleteFiles(files)

	// Test 1: Log files creation test
	loggerFiles, err := createLogFiles()
	if err != nil {
		assert.Failf(t, err.Error(), "Logger Files Creation Error: %v", "Log files creation failed")
	}

	// Test 2: loggerFiles object empty test
	assert.NotNil(t, loggerFiles, "Logger files return nil addresses")

	// Test 3: File exists in directory test
	for _, file := range *files {
		assert.FileExists(t, (*file).path, "File expected to get created, but not found.")
	}

	addTimestamp(files, logger.Sigwait, true)
	// Delete so created test files
	deleteFiles(files)
}
