package main

import (
	log "gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller/app/logger"
)

func main() {
	logger := log.NewLogger()

	logger.Info("Info", "Args")
	logger.Warn("Warn", "Args")
	logger.Error("Error", "Args")

	// time.Sleep(time.Second * 10)
	<-*logger.Signals.Recv
}
