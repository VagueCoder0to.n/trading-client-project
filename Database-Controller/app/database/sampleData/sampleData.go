package sample

type Company struct {
	// gorm.Model

	Name string `json:"company_name"`
	ID   int    `json:"company_id"`
}
type Record struct {
	// gorm.Model

	CompanyID           int
	Date                string  `json:"date"`
	OpenPrice           float64 `json:"open_price"`
	HighPrice           float64 `json:"high_price"`
	LowPrice            float64 `json:"low_price"`
	ClosePrice          float64 `json:"close_price"`
	WAP                 string  `json:"wap"`
	NumberOfShares      int     `json:"number_of_shares"`
	NumberOfTrades      int     `json:"number_of_trades"`
	Turnover            float64 `json:"turnover"`
	DeliverableQuantity float64 `json:"deliverable_quantity"`
	Percentage          float32 `json:"percentage"`
	SpreadHighLow       float64 `json:"spread_high_low"`
	SpreadCloseOpen     float64 `json:"spread_close_open"`
}

type Data []*Record

func NewData() *Data {
	return &Data{
		&Record{
			Date:                "31-Dec-20",
			OpenPrice:           1994.75,
			HighPrice:           2011,
			LowPrice:            1980,
			ClosePrice:          1984.65,
			WAP:                 "1997.539759",
			NumberOfShares:      465795,
			NumberOfTrades:      28122,
			Turnover:            930444032,
			DeliverableQuantity: 135009,
			Percentage:          28.98,
			SpreadHighLow:       31,
			SpreadCloseOpen:     -10.1,
		},
	}
}
