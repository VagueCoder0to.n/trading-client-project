package main

import (
	"encoding/json"

	"gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller/app/database/operations"
	sample "gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller/app/database/sampleData"
	"gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller/app/logger"
)

func main() {
	// logger := log.NewLogger()
	// defer logger.Sigwait.Wg.Wait()

	// mux := http.NewServeMux()
	// serv := http.Server{
	// 	Addr:         ":8080",
	// 	Handler:      mux,
	// 	ReadTimeout:  10 * time.Second,
	// 	WriteTimeout: 30 * time.Second,
	// 	IdleTimeout:  120 * time.Second,
	// }

	// // logger.Info("Data: %v", data)

	// sig := make(chan os.Signal)
	// serv.ListenAndServe()
	// serv.Shutdown()

	logger := logger.NewLogger()
	bytes, err := json.Marshal(sample.NewData())
	if err != nil {
		logger.Error(err)
	}
	logger.Info(string(bytes))

	client := operations.NewDatabaseClient(logger)
	client.Create()

	<-*logger.Signals.Recv
}
