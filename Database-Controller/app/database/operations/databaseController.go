package operations

import log "gitlab.com/VagueCoder0to.n/Trading-Client-Project/Database-Controller/app/logger"

type DatabaseClient struct {
	Logger *log.Logger
}

func NewDatabaseClient(l *log.Logger) *DatabaseClient {
	return &DatabaseClient{
		Logger: l,
	}
}
